package ui.dreamspace.freelance.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ui.dreamspace.freelance.R;
import ui.dreamspace.freelance.adapter.AdapterDayPlansMain;
import ui.dreamspace.freelance.adapter.AdapterDayPlansThree;
import ui.dreamspace.freelance.data.DataGenerator;
import ui.dreamspace.freelance.model.Place;
import ui.dreamspace.freelance.utils.Tools;
import ui.dreamspace.freelance.utils.ViewAnimation;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;

public class DayPlans extends AppCompatActivity {

    // nama variable view lowercase underscore
    RecyclerView recycler_view_image_three, recycler_view_image_main, recycler_view_detail;
    AdapterDayPlansMain adapter_main;
    AdapterDayPlansThree adapter_three, adapter_main_image;
    ImageButton button_detail;
    LinearLayout layout_detail;
    NestedScrollView nested_scroll_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_plans);

        initToolbar();
        initComponent();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Day Plans");
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
    }

    private void initComponent() {
        nested_scroll_view = findViewById(R.id.nested_scroll_view);

        // define recycler view three
        recycler_view_image_three = findViewById(R.id.recycler_view_image_three);
        recycler_view_image_three.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recycler_view_image_three.setHasFixedSize(true);
        recycler_view_image_three.setNestedScrollingEnabled(false);

        List<Place> images_three = DataGenerator.getImage(this);

        //set data and list adapter
        adapter_three = new AdapterDayPlansThree(this, images_three);
        recycler_view_image_three.setAdapter(adapter_three);

        // on item list clicked
        adapter_three.setOnItemClickListener(new AdapterDayPlansThree.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Place obj, int position) {
                Toast.makeText(getApplicationContext(), "Image " + obj.title + " clicked", Toast.LENGTH_SHORT).show();
            }
        });

        // define recycler view main
        recycler_view_image_main = findViewById(R.id.recycler_view_image_main);
        recycler_view_image_main.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recycler_view_image_main.setHasFixedSize(true);
        recycler_view_image_main.setNestedScrollingEnabled(false);

        List<Place> images_main = DataGenerator.getPlacesMain(this);

        //set data and list adapter
        adapter_main_image = new AdapterDayPlansThree(this, images_main);
        recycler_view_image_main.setAdapter(adapter_main_image);

        // on item list clicked
        adapter_main_image.setOnItemClickListener(new AdapterDayPlansThree.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Place obj, int position) {
                Toast.makeText(getApplicationContext(), "Image " + obj.title + " clicked", Toast.LENGTH_SHORT).show();
            }
        });

        button_detail = findViewById(R.id.button_detail);
        layout_detail = findViewById(R.id.layout_detail);

        button_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleSection(view, layout_detail);
            }
        });

        // define recycler view main
        recycler_view_detail = findViewById(R.id.recycler_view_detail);
        recycler_view_detail.setLayoutManager(new LinearLayoutManager(this));
        recycler_view_detail.setHasFixedSize(true);
        recycler_view_detail.setNestedScrollingEnabled(false);

        List<Place> items = DataGenerator.getPlacesMain(this);

        //set data and list adapter
        adapter_main = new AdapterDayPlansMain(this, items);
        recycler_view_detail.setAdapter(adapter_main);

        // on item list clicked
        adapter_main.setOnItemClickListener(new AdapterDayPlansMain.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Place obj, int position) {
                Toast.makeText(getApplicationContext(), "Item " + obj.title + " clicked", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void toggleSection(View bt, final View lyt) {
        boolean show = toggleArrow(bt);
        if (show) {
            ViewAnimation.expand(lyt);
        } else {
            ViewAnimation.collapse(lyt);
        }
    }

    public boolean toggleArrow(View view) {
        if (view.getRotation() == 0) {
            view.animate().setDuration(200).rotation(180);
            return true;
        } else {
            view.animate().setDuration(200).rotation(0);
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_share, menu);
        Tools.changeMenuIconColor(menu, getResources().getColor(R.color.grey_5));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
