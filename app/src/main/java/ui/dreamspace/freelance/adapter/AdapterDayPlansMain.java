package ui.dreamspace.freelance.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import ui.dreamspace.freelance.R;
import ui.dreamspace.freelance.model.Place;
import ui.dreamspace.freelance.utils.Tools;

public class AdapterDayPlansMain extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Place> items;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, Place obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public AdapterDayPlansMain(Context context, List<Place> items) {
        this.items = items;
        ctx = context;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView name;
        public TextView info;
        public TextView open;
        public TextView mode;
        public TextView nomor;
        public View lyt_parent;
        public View lyt_mode;

        public OriginalViewHolder(View v) {
            super(v);
            image = v.findViewById(R.id.image);
            name = v.findViewById(R.id.name);
            info = v.findViewById(R.id.info);
            open = v.findViewById(R.id.open);
            mode = v.findViewById(R.id.mode);
            nomor = v.findViewById(R.id.nomor);
            lyt_parent = v.findViewById(R.id.lyt_parent);
            lyt_mode = v.findViewById(R.id.lyt_mode);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_detail, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            Place p = items.get(position);
            view.nomor.setText(p.nomor);
            view.name.setText(p.title);
            view.info.setText(p.info);
            if(!TextUtils.isEmpty(p.open)){
                view.open.setVisibility(View.VISIBLE);
            }
            view.open.setText(p.open);
            if(!TextUtils.isEmpty(p.mode)){
                view.lyt_mode.setVisibility(View.VISIBLE);
            }
            view.mode.setText(p.mode);
            Tools.displayImageOriginal(ctx, view.image, p.image);
            view.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, items.get(position), position);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}