package ui.dreamspace.freelance.data;

import android.content.Context;
import android.content.res.TypedArray;

import java.util.ArrayList;
import java.util.List;

import ui.dreamspace.freelance.R;
import ui.dreamspace.freelance.model.Place;

@SuppressWarnings("ResourceType")
public class DataGenerator {

    public static List<Place> getPlacesMain(Context ctx) {
        List<Place> items = new ArrayList<>();
        TypedArray drw_arr = ctx.getResources().obtainTypedArray(R.array.place_image);
        String title_arr[] = ctx.getResources().getStringArray(R.array.place_title);
        String info_arr[] = ctx.getResources().getStringArray(R.array.place_info);
        String open_arr[] = ctx.getResources().getStringArray(R.array.place_open);
        String mode_arr[] = ctx.getResources().getStringArray(R.array.place_mode);
        for (int i = 0; i < 3; i++) {
            Place obj = new Place();
            obj.image = drw_arr.getResourceId(i, -1);
            obj.title = title_arr[i];
            obj.info = info_arr[i];
            obj.open = open_arr[i];
            obj.mode = mode_arr[i];
            obj.nomor = String.valueOf(i+1);
            obj.imageDrw = ctx.getResources().getDrawable(obj.image);
            items.add(obj);
        }
        return items;
    }

    public static List<Place> getImage(Context ctx) {
        List<Place> items = new ArrayList<>();
        TypedArray drw_arr = ctx.getResources().obtainTypedArray(R.array.place_image);
        String title_arr[] = ctx.getResources().getStringArray(R.array.place_title);
        for (int i = 3; i < drw_arr.length(); i++) {
            Place obj = new Place();
            obj.image = drw_arr.getResourceId(i, -1);
            obj.title = title_arr[i];
            obj.imageDrw = ctx.getResources().getDrawable(obj.image);
            items.add(obj);
        }
        return items;
    }
}
