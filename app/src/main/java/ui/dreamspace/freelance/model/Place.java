package ui.dreamspace.freelance.model;

import android.graphics.drawable.Drawable;

public class Place {

    public int image;
    public Drawable imageDrw;
    public String title;
    public String info;
    public String open;
    public String mode;
    public String nomor;

    public Place() {
    }

}
